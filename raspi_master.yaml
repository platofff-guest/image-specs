# See https://wiki.debian.org/RaspberryPi3 for known issues and more details.

steps:
  - mkimg: "{{ output }}"
    size: 1500M

  - mklabel: msdos
    device: "{{ output }}"

  - mkpart: primary
    fs-type: 'fat32'
    device: "{{ output }}"
    start: 0%
    end: 20%
    tag: /boot

  - mkpart: primary
    device: "{{ output }}"
    start: 20%
    end: 100%
    tag: /

  - kpartx: "{{ output }}"

  - mkfs: vfat
    partition: /boot
    label: RASPIFIRM

  - mkfs: __ROOT_FS_TYPE__
    partition: /
    label: RASPIROOT

  - mount: /

  - mount: /boot
    mount-on: /
    dirname: '/boot/firmware'

  - unpack-rootfs: /

  - qemu-debootstrap: buster
    mirror: http://deb.debian.org/debian
    target: /
    arch: __ARCH__
    components:
    - main
    - contrib
    - non-free
    unless: rootfs_unpacked

  # TODO(https://bugs.debian.org/877855): remove this workaround once
  # debootstrap is fixed
  - chroot: /
    shell: |
      echo 'deb http://deb.debian.org/debian buster main contrib non-free' > /etc/apt/sources.list
      echo 'deb http://deb.debian.org/debian-security buster/updates main contrib non-free' >> /etc/apt/sources.list
      echo '# Backports are _not_ enabled by default. ' >> /etc/apt/sources.list
      echo '# Enable them by uncommenting the following line:' >> /etc/apt/sources.list
      echo '# deb http://deb.debian.org/debian buster-backports main contrib non-free' >> /etc/apt/sources.list
      apt-get update
    unless: rootfs_unpacked

  - apt: install
    packages:
    - ssh
    - parted
    - dosfstools
    - wireless-tools
    - wpasupplicant
    - raspi3-firmware
    - __LINUX_IMAGE__
    __EXTRA_PKGS__
    tag: /
    unless: rootfs_unpacked

  - cache-rootfs: /
    unless: rootfs_unpacked

  - shell: |
      echo "__HOST__-$(date +%Y%m%d)" > "${ROOT?}/etc/hostname"

      # Allow root logins locally with no password
      sed -i 's,root:[^:]*:,root::,' "${ROOT?}/etc/shadow"

      install -m 644 -o root -g root __FSTAB_FILE__ "${ROOT?}/etc/fstab"

      install -m 644 -o root -g root eth0 "${ROOT?}/etc/network/interfaces.d/eth0"

      install -m 755 -o root -g root rpi-set-sysconf "${ROOT?}/usr/local/sbin/rpi-set-sysconf"
      install -m 644 -o root -g root rpi-set-sysconf.service "${ROOT?}/etc/systemd/system"
      install -m 644 -o root -g root sysconf.txt "${ROOT?}/boot/firmware/sysconf.txt"
      mkdir -p "${ROOT?}/etc/systemd/system/basic.target.requires/"
      ln -s /etc/systemd/system/rpi-set-sysconf.service "${ROOT?}/etc/systemd/system/basic.target.requires/rpi-set-sysconf.service"

      install -m 755 -o root -g root rpi-resizerootfs "${ROOT?}/usr/sbin/rpi-resizerootfs"
      install -m 644 -o root -g root rpi-resizerootfs.service "${ROOT?}/etc/systemd/system"
      mkdir -p "${ROOT?}/etc/systemd/system/systemd-remount-fs.service.requires/"
      ln -s /etc/systemd/system/rpi-resizerootfs.service "${ROOT?}/etc/systemd/system/systemd-remount-fs.service.requires/rpi-resizerootfs.service"

      install -m 644 -o root -g root rpi-generate-ssh-host-keys.service "${ROOT?}/etc/systemd/system"
      mkdir -p "${ROOT?}/etc/systemd/system/multi-user.target.requires/"
      ln -s /etc/systemd/system/rpi-generate-ssh-host-keys.service "${ROOT?}/etc/systemd/system/multi-user.target.requires/rpi-generate-ssh-host-keys.service"
      rm -f ${ROOT?}/etc/ssh/ssh_host_*_key*
    root-fs: /

  # Copy the relevant device tree files to the boot partition
  - chroot: /
    shell: |
      install -m 644 -o root -g root __DTB__ /boot/firmware/

  # Clean up archive cache (likely not useful) and lists (likely outdated) to
  # reduce image size by several hundred megabytes.
  - chroot: /
    shell: |
      apt-get clean
      rm -rf /var/lib/apt/lists

  # Modify the kernel commandline we take from the firmware to boot from
  # the partition labeled raspiroot instead of forcing it to mmcblk0p2
  - chroot: /
    shell: |
      ls -aR /boot
      sed -i 's/.dev.mmcblk0p2/LABEL=RASPIROOT/' /boot/firmware/cmdline.txt

  # TODO(https://github.com/larswirzenius/vmdb2/issues/24): remove once vmdb
  # clears /etc/resolv.conf on its own.
  - shell: |
      rm "${ROOT?}/etc/resolv.conf"
    root-fs: /
